﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katas
{
    public class SupermarketQue
    {
        public static long QueueTime(int[] customers, int n)
        {
            //if only 1 kassa
            if (n == 1)
                return customers.Sum();

            //if more kassas than customers
            if (n > customers.Count())
            {
                return findMax(customers);
            }


            var que = new Queue<int>(customers);
            int[] kassas = new int[n];
            //fill all kassas
            for (int i = 0; i < n; i++)
            {
                kassas[i] = que.Dequeue();
            }
            Console.WriteLine("Filled 2 kassas");

            printKassad(kassas);

            int vabaKassaIndex = 0;
            while (que.Count>0)
            {
                vabaKassaIndex = findMin(kassas);
                kassas[vabaKassaIndex] += que.Dequeue();
                printKassad(kassas);


            }

            //que is over, find max index
            int maxIndex = findMax(kassas);
            Console.WriteLine(maxIndex);
            return maxIndex;
        }

        public static void printKassad(int[] kassad)
        {
            for (int i = 0; i < kassad.Length; i++)
            {
                Console.WriteLine($"Kassa {i} - value {kassad[i]}");
                Console.ReadLine();
            }
        }

        public static int findMin(int[] arr)
        {
            int min = arr[0];
            int minIndex = 0;
            for (int i = 1; i < arr.Length; i++)
            {
                if (min > arr[i])
                {
                    min = arr[i];
                    minIndex = i;
                }
                    

            }
            Console.WriteLine($"Smallest in que is {min}");
            Console.ReadLine();
            return minIndex;
        }

        public static int findMax(int[] arr)
        {
            int max = arr[0];
            foreach (var item in arr)
            {
                if (max < item)
                    max = item;
            }
            return max;
        }
    }
}

/*
There is a queue for the self-checkout tills at the supermarket. 
Your task is write a function to calculate the total time required 
for all the customers to check out!

The function has two input variables:

customers: an array (list in python) of positive integers representing the queue. Each integer represents a customer, and its value is the amount of time they require to check out.
n: a positive integer, the number of checkout tills.
The function should return an integer, the total time required.

There is only ONE queue, and
The order of the queue NEVER changes, and
Assume that the front person in the queue (i.e. the first element 
in the array/list) proceeds to a till as soon as it becomes free.
The diagram on the wiki page I linked to at the bottom of the 
description may be useful.
So, for example:

queueTime([5,3,4], 1)
// should return 12
// because when n=1, the total time is just the sum of the times

queueTime([10,2,3,3], 2)
// should return 10
// because here n=2 and the 2nd, 3rd, and 4th people in the 
// queue finish before the 1st person has finished.

queueTime([2,3,10], 2)
// should return 12
 */

/*
 public static long QueueTime(int[] customers, int n)
{
  var registers = new List<int>(Enumerable.Repeat(0, n));

  foreach(int cust in customers){
    registers[registers.IndexOf(registers.Min())] += cust;
  }
  return registers.Max();
}
 */

/*
 public static long QueueTime(int[] customers, int n)
{
    int[] queues = new int[n];

    foreach(var customer in customers)
    {
        queues[Array.IndexOf(queues, queues.Min())] += customer;
    }

    return queues.Max();
}
 */
