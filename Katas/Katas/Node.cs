﻿using System;

namespace Katas
{
    public class Node
    {
        public int Value;
        public Node Left;
        public Node Right;

        public Node(int value, Node left = null, Node right = null)
        {
            Value = value;
            Left = left;
            Right = right;
        }

        public static int SumTree(Node root)
        {
            return root == null ? 0 : root.Value + SumTree(root.Left) + SumTree(root.Right);
            /*
            int count = root.Value;
            Console.WriteLine($"Count value is {count}");
            if (root.Right != null)
            {
                Console.WriteLine($"Add {root.Right.Value} from RIGHT");
                count += root.Right.Value;
                SumTree(root.Right);
            }
            if (root.Left != null)
            {
                Console.WriteLine($"Add {root.Left.Value} from Left");
                count += root.Left.Value;
                SumTree(root.Left);
            }
            return count;
            */
        }
    }
}


/*write a function that returns the sum of all values, including the root. 
  Absence of a node will be indicated with a null value.

Examples:

10
| \
1  2
=> 13

1
| \
0  0
    \
     2
=> 3
*/