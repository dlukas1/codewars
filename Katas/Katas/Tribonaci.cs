﻿using System.Linq;

namespace Katas
{
    public class Tribonaci
    {
        public double[] Tribonacci(double[] signature, int n)
        {

            // if n==0, then return an array of length 1, containing only a 0
            if (n == 0)
                return new double[] { 0 };

            

            if (n == 1)
                return new double[] { signature[0] };
            if (n == 2)
                return new double[] { signature[0], signature[1] };
            if (n == 1)
                return signature;

            double[] result = new double[n];
            //fill with initial data
            result[0] = signature[0];
            result[1] = signature[1];
            result[2] = signature[2];

            

            double prev = signature[0];
            double mid = signature[1];
            double next = signature[2];
            

            for (int i = 3; i < n; i++)
            {
                double sum = prev + mid + next;
                prev = mid;
                mid = next;
                next = sum;
                result[i] = sum;
              
            }

            return result;
            
        }
    }
}
