﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

/*
What is considered Valid?
A string of braces is considered valid if all braces are matched with the correct brace.

Examples
"(){}[]"   =>  True
"([{}])"   =>  True
"(}"       =>  False
"[(])"     =>  False
"[({})](]" =>  False
*/
/*
public static bool validBraces(string s) {
      string pattern = @"\(\)|\{\}|\[\]";
      while (Regex.IsMatch(s,pattern))
        s = Regex.Replace(s, pattern, "");
      return s == "";
    }
  */
namespace Katas
{
    public static class ValidBraces
    {
        public static bool validBraces(String braces)
        {
            Stack<char> openBraces = new Stack<char>();
            Dictionary<char, char> bracez = new Dictionary<char, char>
            {
                ['('] = ')',
                ['{'] = '}',
                ['['] = ']'
            };
            char[] input = braces.ToCharArray();
            foreach (var brace in input)
            {
                if (bracez.Keys.Contains(brace))
                    openBraces.Push(brace);
                else if(openBraces.Count() != 0 && brace == bracez[openBraces.Pop()])
                    continue;
                else return false;
            }
            return (openBraces.Count() == 0)? true:false;
        }
    }
}
