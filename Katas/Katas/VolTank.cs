﻿using static System.Math;
namespace Katas
{
    public class VolTank
    {
        public static int TankVol(int h, int d, int vt)
        {
            var theta = 2 * Acos(1 - 2d * h / d);
            return (int)(vt / 2d * (theta - Sin(theta)) / PI);

        }
    }
}
/*
To introduce the problem think to my neighbor who drives a tanker truck. 
The level indicator is down and he is worried because he does not know if he 
will be able to make deliveries. We put the truck on a horizontal ground and 
measured the height of the liquid in the tank.

Fortunately the tank is a perfect cylinder and the vertical walls on each 
end are flat. 
-The height of the remaining liquid is h, 
-the diameter of the cylinder is d, 
-the total volume is vt (h, d, vt are positive or null integers). 
You can assume that h <= d.

Could you calculate the remaining volume of the liquid? Your function 
tankvol(h, d, vt) returns an integer which is the truncated result (e.g floor) 
of your float calculation.
*/
