﻿using System;
using System.Collections.Generic;
using System.Text;

/*
Modern Roman numerals are written by expressing each decimal 
digit of the number to be encoded separately, starting with 
the leftmost digit and skipping any 0s. 
So 1990 is rendered "MCMXC" (1000 = M, 900 = CM, 90 = XC) 
and 2008 is rendered "MMVIII" (2000 = MM, 8 = VIII). 
The Roman numeral for 1666, "MDCLXVI", uses each letter in 
descending order.

Example:
RomanDecode.Solution("XXI") -- should return 21
*/

/*
private static Dictionary<char,int> _map3 = new Dictionary<char, int>
        {
            {'I',1},
            {'V',5},
            {'X',10},
            {'L',50},
            {'C',100},
            {'D',500},
            {'M',1000},
        };

    public static int Solution(string roman)
    {
        return roman.Reverse().Aggregate(0, (current, ch) => current + (_map3[ch]*3 < current ? -_map3[ch] : _map3[ch]));
    }
    */

namespace Katas
{
public static class RomanDecoder
{
    public static int Solution(string roman)
    {
        Dictionary<char, int> romans = new Dictionary<char, int>();
        romans.Add('I', 1);
        romans.Add('V', 5);
        romans.Add('X', 10);
        romans.Add('L', 50);
        romans.Add('C', 100);
        romans.Add('D', 500);
        romans.Add('M', 1000);

        char[] arr = roman.ToCharArray();
        int result = 0;
        for (int i = 0; i < arr.Length; i++)
        {
            if(i != arr.Length-1 && romans[arr[i]]< romans[arr[i + 1]])
            {
                result += romans[arr[i + 1]] - romans[arr[i]];
                i++;
            }

            else
                result += romans[arr[i]];


        }
        return result;
    }
}
}
