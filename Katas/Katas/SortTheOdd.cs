﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Katas
{
    public class SortTheOdd
    {
        public static int[] SortArray(int[] array)
        {
            if (array.Length == 0)
                return array;

            for (int i = 0; i < array.Length; i++)
            {
                if (array[i] % 2 == 0)
                    continue;
                else
                {
                    // array = InsertionSort(array, i);
                    for (int j = 0; j < array.Length-1; j++)
                    {
                        if (array[j] % 2 == 0)
                            continue;
                        else if (array[j] < array[i])
                            continue;
                        else
                        {
                            int temp = array[j];
                            array[j] = array[i];
                            array[i] = temp;
                        }                        
                    }
                }
            }

            return array;
        }

        public static int[] InsertionSort(int[] inputArray, int index)
        {
                for (int j = index + 1; j > 0; j--)
                {
                    if (inputArray[j - 1] > inputArray[j])
                    {
                        int temp = inputArray[j - 1];
                        inputArray[j - 1] = inputArray[j];
                        inputArray[j] = temp;
                    }
                }           
            return inputArray;
        }

        public static void printArray(int[] arr)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var c in arr)
            {
                sb.Append(" " + c + " ");
            }
            Console.WriteLine(sb.ToString());
            Console.ReadLine();
        }
    }
}
/*
public static int[] SortArray(int[] array)
    {
        Queue<int> odds = new Queue<int>(array.Where(e => e%2 == 1).OrderBy(e => e));
    
        return array.Select(e => e%2 == 1 ? odds.Dequeue() : e).ToArray();
    }
    */

/*
You have an array of numbers.
Your task is to sort ascending odd numbers but even numbers must be on their places.

Zero isn't an odd number and you don't need to move it. If you have an empty array, you need to return it.

Example

sortArray([5, 3, 2, 8, 1, 4]) == [1, 3, 2, 8, 5, 4]
*/
