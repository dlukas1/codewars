﻿using System;

namespace Katas
{
    public class VasyaClerk
    {
        public static string Tickets(int[] peopleInLine)
        {
            int ticketPrice = 25;
            if (peopleInLine[0] != ticketPrice)
                return "NO";
            
            int bill25 = 0;
            int bill50 = 0;

            foreach (var bill in peopleInLine)
            {
                if (bill == ticketPrice)
                {
                    bill25++;
                    continue;
                }
                else if(bill == 50)
                {
                    bill50++;
                    if (bill25==0)
                    {
                        return "NO";
                    }
                    else
                    {
                        bill25--;
                        continue;
                    }
                }
                else 
                
                if (bill == 100)
                {
                    if (bill50 > 0 && bill25 > 0)
                    {
                        bill50--;
                        bill25--;
                        continue;
                    }
                    else if (bill25 > 2)
                    {
                        bill25--;
                        bill25--;
                        bill25--;
                        continue;
                    }
                    else return "NO";
                }                  
            }
            return "YES";
        }
    }
}
/*
The new "Avengers" movie has just been released! There are a lot of people at the cinema box office standing in a huge line. Each of them has a single 100, 50 or 25 dollars bill. An "Avengers" ticket costs 25 dollars.

Vasya is currently working as a clerk. He wants to sell a ticket to every single person in this line.

Can Vasya sell a ticket to each person and give the change if he initially has no money and sells the tickets strictly in the order people follow in the line?

Return YES, if Vasya can sell a ticket to each person and give the change with the bills he has at hand at that moment. Otherwise return NO.

###Examples:

// === C Sharp ===

Line.Tickets(new int[] {25, 25, 50}) // => YES 
Line.Tickets(new int[] {25, 100}) // => NO. Vasya will not have enough money to give change to 100 dollars
Line.Tickets(new int[] {25, 25, 50, 50, 100}) // => NO.
*/
/*
public static string Tickets(int[] p)
        { 
            int m25=0,m50=0;
            for (int i=0;i<p.Length&m25>=0;i++){
              m25+=(p[i]==25 ? 25 : p[i]==50 ? -25 : m50<50 ? -75 : -25);
              m50+=(p[i]==25 ? 0  : p[i]==50 ?  50 : m50<50 ?   0 : -50);
            }
            return m25<0 ? "NO" : "YES";
        }
*/