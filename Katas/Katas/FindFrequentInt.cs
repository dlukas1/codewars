﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katas
{
    public class FindFrequentInt
    {
        public static int HighestRank(int[] arr)
        {
            return arr
                .GroupBy(i => i)
                .OrderByDescending(gr => gr.Count())
                .ThenByDescending(gr => gr.Key)
                .Select(gr => gr.Key)
                .First();
        }
    }
}
/*
 * Write a method highestRank(arr) (or highest-rank in clojure) which returns the number which is most frequent in the given input array (or ISeq). 
 * If there is a tie for most frequent number, return the largest number of which is most frequent.
Example:
var arr = new int[] { 12, 10, 8, 12, 7, 6, 4, 10, 12 };
Kata.HighestRank(arr) //=> returns 12
 */

/*
return arr
            .GroupBy(i => i)    //group items by value
            .OrderByDescending(gr => gr.Count())  //order by elements in each group
            .ThenByDescending(gr => gr.Key) //order by value
            .Select(gr => gr.Key)   //select value
            .First();
*/