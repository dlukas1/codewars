﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katas
{
    public class WhichAreIn
    {
        public static string[] inArray(string[] array1, string[] array2)
        {
            List<string> result = new List<string>();
            foreach (var sub in array1)
            {
                foreach (var word in array2)
                {
                    if (word.Contains(sub))
                        result.Add(sub);
                }
            }


            return result.Distinct().ToArray();
        }
    }
}

/*Given two arrays of strings a1 and a2 return a sorted array r in lexicographical order of the strings of a1 which are substrings of strings of a2.

#Example 1: a1 = ["arp", "live", "strong"]

a2 = ["lively", "alive", "harp", "sharp", "armstrong"]

returns ["arp", "live", "strong"]
*/
/*
 return array1.Distinct()
                              .Where(s1 => array2.Any(s2 => s2.Contains(s1)))
                              .OrderBy(s => s)
                              .ToArray();
*/