﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Katas
{
    public static class ParityOutlier
    {
     

        public static int Find(int[] integers)
        {
            //size  >= 3
            bool isOutlierEven = true;
            int nEven = 0;
            for (int i = 0; i < 3; i++)
            {
                if (integers[i] % 2 == 0)
                    nEven++;
            }
            if (nEven > 1)
                isOutlierEven = false;

            switch (isOutlierEven? 1:2)
            {
                case 1:
                    foreach (int item in integers)
                    {
                        if (item % 2 == 0)
                            return item;                      
                    }
                    break;

                case 2:
                    foreach (int item in integers)
                    {
                        if (item % 2 == 1)
                            return item;
                    }
                    break;
            }
            return -1;
        }
    }
}


/*
 You are given an array (which will have a length of at least 3, but could be very large) containing integers. The array is either entirely comprised of odd integers or entirely comprised of even integers except for a single integer N. Write a method that takes the array as an argument and returns this "outlier" N.

Examples
[2, 4, 0, 100, 4, 11, 2602, 36]
Should return: 11 (the only odd number)

[160, 3, 1719, 19, 11, 13, -21]
Should return: 160 (the only even number)
 */
/*
        public static int Find(int[] integers)
         {
           var evenNumbers = integers.Where(integer => integer % 2 == 0);
           var oddNumbers = integers.Where(integer => integer % 2 == 1);
           return evenNumbers.Count() == 1 ? evenNumbers.First() : oddNumbers.First();
         }
        */
/*
 public static int Find(int[] arr)
  {
      int n = arr.Take(3).Count(i => i%2 == 0) > 1 ? 1 : 0;
      return arr.First(i => i%2 == n);
  }
     */

/*
 public static int Find(int[] integers)
{
    return integers.GroupBy(x => x & 1).OrderBy(g => g.Count()).First().Single();
}
 */
