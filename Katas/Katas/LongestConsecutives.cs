﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Katas
{
    public static class LongestConsecutives
    {
        public static String LongestConsec(string[] strarr, int k)
        {
            int currentMax = 0;
            string currentRes = "";
            int len = 0;
            string res = "";

            for (int i = 0; i < strarr.Length+1-k; i++)
            {
                for (int j = 0; j < k; j++)
                {
                    res += strarr[i + j];
                }
                len = res.Length;

                // Compare
                if (len > currentMax)
                {
                    currentMax = len;
                    currentRes = res;
                }

                
                res = "";
            }
            Console.WriteLine(currentRes);
            Console.ReadLine();

            
            return currentRes;
        }
    }
}
/*
You are given an array strarr of strings and an integer k. 
Your task is to return the first longest string consisting of k consecutive strings taken in the array.
#Example: longest_consec(["zone", "abigail", "theta", "form", "libe", "zas", "theta", "abigail"], 2) --> "abigailtheta"

n being the length of the string array, if n = 0 or k > n or k <= 0 return "".
     */
