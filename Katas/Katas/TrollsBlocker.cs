﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Katas
{
    public class TrollsBlocker
    {
        public static string Disemvowel(string str)
        {

            //char[] vovels = new char[] { 'a', 'e', 'i', 'o', 'u' };
            str = System.Text.RegularExpressions.Regex.Replace(str, @"[a,e,o,i,u]", "");
            return str;
            
        }
    }
}
/*
 Trolls are attacking your comment section!
A common way to deal with this situation is to remove all of the vowels from the trolls' comments, neutralizing the threat.
Your task is to write a function that takes a string and return a new string with all vowels removed.
For example, the string "This website is for losers LOL!" would become "Ths wbst s fr lsrs LL!".
Note: for this kata y isn't considered a vowel.
 */
